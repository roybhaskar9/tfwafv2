resource "aws_wafv2_web_acl" "main" {
  
  name  = "example"
  scope = "REGIONAL"

  default_action {
    allow {}
  }

  rule {
    
      name     = "AWSManagedRulesCommonRuleSet-rule-1"
      priority = 1

      override_action {
        none {}
      }

      statement {
        managed_rule_group_statement {
            name        = "AWSManagedRulesCommonRuleSet"
            vendor_name = "AWS"

        excluded_rule {
                name = "SizeRestrictions_QUERYSTRING"
              }
            }
          }
        

	visibility_config {
          cloudwatch_metrics_enabled = "false"
          metric_name                = "AWSManagedRulesCommonRuleSet-metric"
          sampled_requests_enabled   = "false"
        }
      }
    
  visibility_config {
      cloudwatch_metrics_enabled = "false"
      metric_name                = "test-waf-setup-waf-main-metrics"
      sampled_requests_enabled   = "false"
    }
  }
